CREATE TABLE `units` (
  `unit_id` int(11) NOT NULL,
  `unit_name` varchar(100) NOT NULL,
  `unit_short_code` varchar(10) NOT NULL,
  `course_id` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1