INSERT INTO `scitm`.`staff_groups` (`group_name`, `description`) VALUES ('ADMINISTRATIVE', 'Will be the administrative portion of the school');
INSERT INTO `scitm`.`staff_groups` (`group_name`, `description`) VALUES ('MANAGERIAL', 'Will be the heads of the school');
INSERT INTO `scitm`.`staff_groups` (`group_name`, `description`) VALUES ('TEACHING', 'Are the college lecturers');
INSERT INTO `scitm`.`staff_groups` (`group_name`, `description`) VALUES ('SUPPORT', 'Are the subordinate staff');
