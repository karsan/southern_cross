<!DOCTYPE html>
<html lang="en">
<head>
<title>About Us</title>
<meta charset="utf-8">
<meta name="format-detection" content="telephone=no" />
 <link rel="stylesheet" type="text/css" href= "<?php echo base_url(). 'assets/flat-ui/bootstrap/css/bootstrap.css'?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(). 'assets/flat-ui/css/flat-ui.css'?>" />
<link rel="<?php echo base_url() .'assets/css/icons/icon.css'?>" href="<?php echo base_url() .'assets/images/favicon.ico'?>">
<link rel="shortcut icon" href="<?php echo base_url() .'assets/images/favicon.ico'?>" />
<link rel="stylesheet" href="<?php echo base_url() .'assets/font-awesome/css/font-awesome.css'?>">
<link rel="stylesheet" href="<?php echo base_url() .'assets/css/home.css'?>">



<!--[if lt IE 8]>
 <div style=' clear: both; text-align:center; position: relative;'>
   <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
     <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
   </a>
</div>
<![endif]-->
<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<link rel="stylesheet" media="screen" href="css/ie.css">
<![endif]-->
</head>
<body>
<!--==============================
              header
=================================-->
<header class="hed">
  <div class="container">
    <div class="row">
      <div class="grid_12 rel">
        <h1>
          <a href="index.html">
            <img src="<?php echo base_url() .'assets/images/siteimage.jpg'?>" alt="Logo alt">
          </a>
        </h1>
      </div>
    </div>
  </div>
   <section id="stuck_container">
  <!--==============================
              Stuck menu
  =================================-->
    <div class="container" class="not_home">
      <div class="row">
        <div class="grid_12 ">
          <div class="navigation">
            <nav>
              <ul class="sf-menu">
               <li><a href="<?php echo base_url(). 'home/index'?>">Home</a></li>
               <li><a href="<?php echo base_url(). 'announcements/announce'?>">Programs</a></li>
               <li><a href="<?php echo base_url(). 'home/contacts'?>">Contacts</a></li>
               
               <!-- <li class="current"><a href="#">Contacts</a></li> -->
               
               <li><a href="#">About</a></li>
               <li class="white-text"><a href="#" class="" data-toggle="modal" data-target="#login_modal">Login</a></li>
             </ul>
            </nav>
            <div class="clear"></div>
          </div>       
         <div class="clear"></div>  
        </div>
     </div> 
    </div> 
  </section>
</header>
<!--=====================
          Content
======================-->
<section id="content"><div class="ic">SouthernCross @ www.southerncrossinstitute.edu - July 28, 2014!</div>
<!--Login modal -->
<div class="modal fade" id="login_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Login</h4>
      </div>
      <div class="modal-body">
      <?php $attr = array('id' =>"login_form",'name' => "login_form"); echo form_open("users/login",$attr); ?>
      <label for="user_name">User name:* </label>
      <input type="text" class="input form-control user_name" name="user_name" required title="Please fill in the required fields">
      <label for="password">Password:* </label>
      <input type="password" class="input form-control password" name="password" required title="Please fill in the required fields">
      <p class="warning_msg">**Please ensure all fields are filled**</p>
      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary margin-right-sm login_button">Login</button>
        <?php echo form_close(); ?>
      </div>
  </div>
</div>
</div>
  <div class="container">
    <div class="row">
      <div class="grid_12">
        <h3>Mission</h3>    
      </div>
      <div class="grid_3">
        <div class="text2 color1"><a href="#">Specialized Training</a></div>To provide not just education, but specialized training to make our students experts in their primary medical fields while maintaining high levels of professionalism, honesty and integrity
      </div>
      <div class="grid_2">
        <div class="text2 color1"><a href="#">Vision</a></div>We are committed to being the most sort after mid-level) medical training college in Kenya and beyond
      </div>
      <div class="grid_3">
        <div class="text2 color1"><a href="#">Medical Standards</a></div>Providing tertiary education that meets the current trends in medicare provisions in Kenya and beyond)
      </div>
      <div class="grid_3">
        <div class="text2 color1"><a href="#">Continuity</a></div>We are guided by and strive to uphold effective corporate governance, objective policies formulation, constructive networking, professionalism and sound management, with emphasis on exceeding customer satisfaction, operational functionality, efficientservice delivery and cost-effectiveness for sustained growth
      </div>
      <!-- <div class="grid_2">
        <div class="text2 color1"><a href="#">Еestibu dertolo</a></div>Curabitur vel lorem sitmet nulla ullamcorper mentum In vitae dert rius augue, eu consectetur ligulaam dui eros dertolisce dertoloing quam id risus sagittis
      </div>
      <div class="grid_2">
        <div class="text2 color1"><a href="#">Nolutpaersa fert</a></div>Curabitur vel lorem sitmet nulla ullamcorper mentum In vitae dert rius augue, eu consectetur ligulaam dui eros dertolisce dertoloing quam id risus sagittis
      </div> -->
    </div>
  </div>
  <article class="content_gray offset__2">
    <div class="container">
      <div class="row">
        <div class="grid_3">
          <h3>Undertaking</h3>
          <ul class="list-1">
            <li><a href="#">Provide first class training for Community Health Workers ), Clinical Officers, Registered Nurses and other Mid-Level Medical Practitioners</a></li>
            <li><a href="#">Engage and sustain a highly qualified staff in academics and administration</a></li>
            <li><a href="#">Provide consistent and reliable service to students and sponsors</a></li>
            <li><a href="#">Meet Service Level Agreements</a></li>
            
          </ul>
        </div>
        <div class="grid_4">
          <h3>Keys To Success</h3>
          <div class="block-2">
            <!-- <img src="images/page3_img1.jpg" alt="" class="img_inner fleft"> -->
            <div class="extra_wrapper">
              
                <p>• Provide market-oriented programs</p>
                <p>• Provide high academic standards</p>
                <p>• Make research an integral part of learning</p>
                <p>• Create a good network for student placements in the job market</p>

            </div>
          </div>
          <!-- <div class="block-2 offset__1">
            <img src="images/page3_img2.jpg" alt="" class="img_inner fleft">
            <div class="extra_wrapper">
              <div class="text1"><a href="#">Olquam nibh ante</a></div>
              Trabitur vel lorem sit amet nulla ullamcorper fermentum In vitae varius augue, eu consectetur ligulaam dui eroserty.
            </div>
          </div> -->
        </div>
        <div class="grid_3 preffix_1">
          <h3>Objectives</h3>
          <div class="block-2">
              <!-- <div class="text1"><a href="#"> Ipsumen Dertol</a></div> -->
                <p>• Become the premier mid-level medical college in East and Central Africa</p>
                <p>• Become the centre of research and academic excellence</p>

          </div>
          <!-- <div class="block-2 ">
              <div class="text1"><a href="#">Setorem Osumen</a></div>
              Nurabitur vel lorem sit amet nulla corper fermentum In vitae varius augue, eu ctetur ligulaam dui eroserty. Fusce adipiscing quam id risus sagittis, non consequat lacus  <br> <a href="#" class="link-1">more</a> 
          </div> -->
        </div>
      </div>
    </div>
  </article>
  <div class="container">
    <div class="row">
      <div class="grid_12">
        <h3>Values</h3>
      </div>
      <div class="grid_4">
        <!-- <img src="images/page3_img3.jpg" alt="" class="img_inner fleft"> -->
        <div class="extra_wrapper">
          <p class="fwn"><a href="#">Our Management Principles</a></p>
          <p class="offset__1">--</p>
        </div>
          <p>1.  Committing to client satisfaction as our most important business objective</p>
          <p>2.  Maintaining the highest standards of professionalism and academic excellence</p>
          <p>3.  Maintaining the highest standards of ethics and business conduct and operating at all times within the laws of Kenya and the by-laws of counties in which we do business</p>
          <p>4.  Encourage initiative, recognize individual contribution, and treat each person with respect and fairness</p>
          <p>5.  Identifying and responding aggressively to new opportunities, and committing to success in each undertaking</p>

      </div>
      <div class="grid_4">
        <!-- <img src="images/page3_img4.jpg" alt="" class="img_inner fleft"> -->
        <div class="extra_wrapper">
          <p class="fwn"><a href="#">Our Team</a></p>
          <p class="offset__1">--</p>
        </div>
           Backed by highly skilled and professional staff, which is our greatest resource, we are able to deliver outstanding performance through demonstrated experience & expertise in all aspects of Academics and Management of the Medical College
      </div>
      <div class="grid_3">
        <!-- <img src="images/page3_img5.jpg" alt="" class="img_inner fleft"> -->
        <div class="extra_wrapper">
          <p class="fwn"><a href="#">Organization Structure</a></p>
          <p class="offset__1">--</p>
        </div>
        <!-- <p>1.  Executives</p>
        <p>2.  Qualified Lecturers</p>
        <p>3.  Teaching Professionals</p>
        <p>4.  Administrative Professionals</p>
        <p>5.  Skilled Manpower</p> -->
     <img width="450px" height="300px" src="<?php echo base_url() .'assets/images/Organization.jpg'?>" alt="" class="img_inner fleft">
      </div>
      <!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal2">
  Click for Larger View
</button>

<!-- Modal -->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content modcont">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Organizational Structure</h4>
      </div>
      <div  class="modal-body">
        <img src="<?php echo base_url() .'assets/images/Organization.jpg'?>" alt="" class="img_inner fleft">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>


    </div>
  </div>
</section>
<!--==============================
              footer
=================================-->
<footer id="footer">
  <div class="container">
    <div class="row">
      <div class="grid_12"> 
        <div class="copyright"><span class="brand">SouthernCrossInstitute</span> &copy; <span id="copyright-year"></span> | <a href="#">Privacy Policy</a>
          <div class="sub-copy">www.southerncrossinstitute.edu</div>
        </div>
      </div>
    </div>
  </div>  
</footer>
<a href="#" id="toTop" class="fa fa-chevron-up"></a>


<script src="<?php echo base_url() .'assets/js/jquery.js'?>"></script>
<script src="<?php echo base_url() .'assets/js/jquery-migrate-1.1.1.js'?>"></script>
<script src="<?php echo base_url() .'assets/js/jquery.easing.1.3.js'?>"></script>
<script src="<?php echo base_url(). 'assets/flat-ui/js/bootstrap.min.js'?>"></script>
      <script src="<?php echo base_url(). 'assets/flat-ui/js/bootstrap-select.js'?>"></script>
      <script src="<?php echo base_url(). 'assets/flat-ui/js/bootstrap-switch.js'?>"></script>
      <script src="<?php echo base_url(). 'assets/flat-ui/js/flatui-checkbox.js'?>"></script>
      <script src="<?php echo base_url(). 'assets/flat-ui/js/flatui-radio.js'?>"></script>
<script src="<?php echo base_url() .'assets/js/home.js'?>"></script> 
<script src="<?php echo base_url() .'assets/js/superfish.js'?>"></script>
<script src="<?php echo base_url() .'assets/js/jquery.equalheights.js'?>"></script>
<script src="<?php echo base_url() .'assets/js/jquery.mobilemenu.js'?>"></script>
<script src="<?php echo base_url() .'assets/js/tmStickUp.js'?>"></script>
<script src="<?php echo base_url() .'assets/js/jquery.ui.totop.js'?>"></script>
<script src="<?php echo base_url(). 'assets/js/script.js'?>"></script>

<script>
 $(window).load(function(){
  $().UItoTop({ easingType: 'easeOutQuart' });
  $('#stuck_container').tmStickUp({});  
 }); 
</script>
</body>
</html>