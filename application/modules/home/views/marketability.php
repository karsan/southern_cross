<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
    <head>
        <title>Marketability</title>
        
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link rel="stylesheet" type="text/css" href= "<?php echo base_url(). 'assets/flat-ui/bootstrap/css/bootstrap.css'?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(). 'assets/flat-ui/css/flat-ui.css'?>" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>
        <link href="<?php echo base_url(). 'assets/css/animate.css'?>" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url(). 'assets/css/slippry.css'?>">
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(). 'assets/css/jquery.mmenu.all.css'?>" />
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(). 'assets/css/style.css'?>" />
        <link href="<?php echo base_url(). 'assets/css/style3.css'?>" rel='stylesheet' type='text/css' />
        
    </head>
    <body>
        <!---start-wrap---->
            <!---start-banner---->
            <div class="banner" id="move-top">
                <!----start-header---->
                <div class="header animated bounceInDown">
                    <!------start-768px-menu---->
                        <div id="page">
                                <div id="header">
                                    <a class="navicon" href="#menu-left"> </a>
                                </div>
                                <nav id="menu-left">
                                    <ul>
                                        <li><a href="<?php echo base_url() . 'home/index'?>">Home</a></li>
                                        <li><a href="<?php echo base_url() . 'home/about'?>">Preamble</a></li>
                                <li><a href="<?php echo base_url() . 'home/services'?>">About Us</a></li>
                                <li><a href="<?php echo base_url() . 'home/contacts'?>">Contact</a></li>
                                    </ul>
                                </nav>
                            </div>
                    <!------start-768px-menu---->
                    <div class="wrap">
                        <!---start-logo---->
                        <div class="logo" style="color:#fff; font-size:14px">
                            <!-- <a data-scroll-reveal="enter from the top over 0.7s" href="#"> --> Southern Cross : Marketability<!-- <img src="<?php echo base_url(). 'assets/images/logo.png'?>" title="KreativePixel" /> --><!-- </a> -->
                        </div>

                        <!---//End-logo---->
                        <!---start-top-nav---->
                        <div data-scroll-reveal="enter from the top over 0.7s" class="top-nav">
                            <ul>
                                <li class="active"><a href="<?php echo base_url() . 'home/index'?>">Home<span> </span></a></li>
                                <li><a href="<?php echo base_url() . 'home/about'?>">Preamble</a></li>
                                <li><a href="<?php echo base_url() . 'home/services'?>">About Us</a></li>
                                <li><a href="<?php echo base_url() . 'home/contacts'?>">Contact</a></li>
                            </ul>
                        </div>
                        <div class="clear"> </div>
                        <!---//End-top-nav---->
                    </div>
                </div>
                <!----//End-header---->
                <!----start-image-slider---->
                    <div data-scroll-reveal="enter bottom but wait 0.7s" class="img-slider" id="home">
                        <div class="wrap">
                            <ul id="jquery-demo">
                              <li>
                                <a href="#slide1">
                                </a>
                                <div data-scroll-reveal="enter bottom but wait 0.7s" class="slider-detils">
                                    The health workforce is a vital component of any health system. <br />Moving from a disease-specific approach to healthcare promotion, <br />policy makers are now shifting towards a health systems approach,<br /> which includes strengthening the health workforce. <br />The impetus for this new focus on
                                     health systems strengthening and<br /> human resources for health (HRH) came, in part, <br />
                                    as a result of research from the 2004 Joint<br /> Learning Initiative and The World Health Report in 2006, <br />which demonstrated a clear 
                                   link between<br /> health workforce density and a number of health indicators (2004; 2006b).
                                    <!-- <a class="slide-btn" href="#">View Projects</a> -->
                                </div>
                              </li>
                              <li>
                                <a href="#slide2">
                                </a>
                                  <div data-scroll-reveal="enter bottom but wait 0.7s" class="slider-detils">
                                    Numerous studies have explored the link between an adequate<br /> supply and deployment of HRH and health services delivery. <br />The Joint Learning Initiative, comprised of global health experts, 
                                       found that <br />a density of 2.3 health care workers per<br /> 1,000 population was associated with 80% coverage <br />in skilled birth attendance and measles vaccination (2004), <br />and a distinct 
                                    relationship between the density of the health workforce and<br /> mortality rates for mothers, infants and children under five. <br />However, thirty-six sub-Saharan African countries, including 
                                      Kenya,<br /> are facing a critical shortage of heath care workers. 
                                    <!-- <a class="slide-btn" href="#">View Projects</a> -->
                                </div>
                              </li>
                              <li>
                                <a href="#slide3">
                                </a>
                                  <div data-scroll-reveal="enter bottom but wait 1.5s" class="slider-detils">
                                    The Government of Kenya (GoK), in partnership with the<br /> President's Emergency Plan for AIDS Relief (PEPFAR), is<br /> considering interventions to scale-up Kenya's health workforce,<br /> increasing the number of doctors, nurses, midwives,<br /> clinical 
                                    officers and community workers, among other cadres. <br />It should be noted that registration as a professional health worker requires <br />enrolling in and completing an approved training <br />
                                    institution (i.e. intake and graduation), taking a licensure exam,<br /> and applying for registration. <br />Kenya's Health Workforce Information on workforce registration and <br />licensure renewals can be used to calculate Kenya's<br /> workforce-to-population density and 
                                    compare it to the<br /> WHO recommendation of 2.3 health workers per 1,000 populations.
                                    <!-- <a class="slide-btn" href="#">View Projects</a> -->
                                </div>
                              </li>
                              <li>
                                <a href="#slide4">
                                </a>
                                  <div data-scroll-reveal="enter bottom but wait 1.5s" class="slider-detils">
                                    <table style="color:#000" class="table modal-table">
                                       <tr style="background-color:#0099CC">
                                         <strong class="str"><td style="text-align:center" 

colspan="7">Table 1: Kenya's Health WorkForce</td></strong>
  </tr>
  <tr style="background-color:#0099CC">
    <strong>
    <td></td>
    <td>Doctors</td>
    <td>Dentists</td>
    <td>Nurses/Mid-Wives</td>
    <td>Clinical Officers</td>
    <td>Laboratory Techs</td>
    <td>Total</td>
    </strong>
  </tr>
  <tr style="background-color:#66FFFF">
    <td>Registered</td>
    <td>6,306</td>
    <td>780</td>
    <td>43,970</td>
    <td>8,300</td>
    <td>4,699</td>
    <td>64,055</td>
  </tr>
  <tr style="background-color:#66FFFF">
    <td>Licensure Renewals</td>
    <td>4,756</td>
    <td>590</td>
    <td>28,214</td>
    <td>6,300</td>
    <td>2,092</td>
    <td>41,952</td>
  </tr>
</table>
                                </div>
                              </li>
                              <li>
                                <a href="#slide5">
                                </a>
                                  <div data-scroll-reveal="enter bottom but wait 1.5s" class="slider-detils">
                                    Calculating workforce-to-population ratios based on both <br/ >the number of "registered" and "retained" health care workers<br/ > provides a range for workforce density estimates. <br/ >According to 
   the number of registered health personnel,<br/ > Kenya has 1.54 health workers (doctors, nurses, midwives,<br/ > and clinical officers) per 
  1,000 people. <br/ >Based on the number of retained health personnel,<br/ > Kenya has 1.03 health workers (doctors, nurses, midwives, <br/ >and 
    clinical officers) per 1,000 people <br/ >based on the 2009 Census data for an estimated national <br/ >population of 38.6M in 2010. <br/ >Using 
  either estimate, the density of health workers <br/ >in Kenya is below the recommended 2.30 per 1,000 people.  
                                </div>
                              </li>
                            </ul>
                        </div>
                    </div>
                    <div class="clear"> </div>
                </div>
                    <!----//End-image-slider---->
                                        <!---start-footer--->
                    <!----start-scrooling-script---->
                                 <a href="#move-top" id="toTop" style="display: block;"><span id="toTopHover" style="opacity: 1;"> </span></a>
                            <!----//End-scrooling-script---->
                    <div data-scroll-reveal="enter bottom but wait 0.7s" class="footer">
                        <div data-scroll-reveal="enter bottom but wait 0.7s" class="footer-top">
                            <div class="wrap">
                                <ul>
                                    <li><a href="<?php echo base_url(). 'home/index'?>">Back Home</a></li>
                                    <li><a href="<?php echo base_url() . 'home/about'?>">Preamble</a></li>
                                <li><a href="<?php echo base_url() . 'home/services'?>">About Us</a></li>
                                <li><a href="<?php echo base_url() . 'home/contacts'?>">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="footer-bottom">
                            <div class="wrap">
                                <p>Website by <a href="http://continuumdevelopers.com/">Continuum Developers</a></p>
                            </div>
                        </div>
                    </div>
                   
<script src="<?php echo base_url() .'assets/bootstrap/js/bootstrap.js'?>"></script>
<script src="<?php echo base_url(). 'assets/flat-ui/js/bootstrap-select.js'?>"></script>
      <script src="<?php echo base_url(). 'assets/flat-ui/js/bootstrap-switch.js'?>"></script>
      <script src="<?php echo base_url(). 'assets/flat-ui/js/flatui-checkbox.js'?>"></script>
      <script src="<?php echo base_url(). 'assets/flat-ui/js/flatui-radio.js'?>"></script>
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<script src="<?php echo base_url(). 'assets/js/jquery.min_2.js'?>"></script>
 <script src="<?php echo base_url(). 'assets/js/jquery-ui_2.js'?>" type="text/javascript"></script>
        <script src="<?php echo base_url(). 'assets/js/scripts-f0e4e0c2.js'?>" type="text/javascript"></script>
        <script>
              jQuery('#jquery-demo').slippry({
              // general elements & wrapper
              slippryWrapper: '<div class="sy-box jquery-demo" />', // wrapper to wrap everything, including pager
              // options
              adaptiveHeight: false, // height of the sliders adapts to current slide
              useCSS: false, // true, false -> fallback to js if no browser support
              autoHover: false,
              transition: 'fade'
            });
        </script>
        <!---scrooling-script--->
        <!---smoth-scrlling---->
                            <script type="text/javascript">
                                    $(document).ready(function(){
                                    $('a[href^="#"]').on('click',function (e) {
                                        e.preventDefault();
                                        var target = this.hash,
                                        $target = $(target);
                                        $('html, body').stop().animate({
                                            'scrollTop': $target.offset().top
                                        }, 1000, 'swing', function () {
                                            window.location.hash = target;
                                        });
                                    });
                                });
                            </script>

                   <script type="text/javascript" src="<?php echo base_url(). 'assets/js/jquery.mmenu.js'?>"></script>
            <script type="text/javascript">
                //  The menu on the left
                $(function() {
                    $('nav#menu-left').mmenu();
                });
        </script>
    </body>
</html>

