<html lang="en">
<head>
    
    <title>Certififcate Application Form</title>
    <link rel="icon" type="image/x-icon" href="<?php echo base_url(). 'assets/icons/hospital.ico'?>" />
    <link rel="stylesheet" type="text/css" href= "<?php echo base_url(). 'assets/bootstrap/css/bootstrap.css'?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(). 'assets/kickstart/css/fonts/font-awesome/css/font-awesome.min.css'?>">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(). 'assets/script/jquery/jquery-ui.css'?>"/>
    <link rel="stylesheet" href="<?php echo base_url(). 'assets/stylesheets/animate.css'?>">
      <link rel="stylesheet" type="text/css" href= "<?php echo base_url(). 'assets/css/custom.css'?>">
      <link rel="stylesheet" type="text/css" href= "<?php echo base_url(). 'assets/stylesheets/style.css'?>">
    
</head>
<body>
  <div class=" content container ">

<div class=" form_header">
  <div class=" top_logo clearfix">
    <div class = "blue_strip clearfix"></div>
    <div class=" light_blue_strip clearfix"></div>
    <center><img class="clearfix" src="<?php echo base_url(). 'assets/images/siteimage.jpg'?>"></center>
    <!-- 
    <center><p class="top_header clearfix ">SOUTHERN CROSS</p></center>
    <center><p class="top_mini_header clearfix">INSTITUTE OF TROPICAL MEDICINE</p></center>
     -->
    <div class="form_title"><strong><h2>CERTIFICATE APPLICATION FORM</h2></strong></div>
    <div class=" logo_information">
    </div>
    
    <div class=" logo_more_information float_left">
      <p><strong class=" float_left clearfix clear_right">Address: </strong> P.O. Box 1280</p>
      <p>Kisumu,Kenya</p>
      <p><strong class=" float_left clearfix">Email: </strong>  southerncrossinstitute@gmail.com</p>
    </div>
    <div class="logo_more_information float_right">
    <p><strong class=" float_left clearfix clear_right">Tel Office: </strong> (+254) 0717 - 056 523</p>
    <p><strong class=" float_left clearfix clear_right">Mobile: </strong> (+254) 0717 - 056 523</p>
    </div>

</div>


    <nav class="navbar navbar-inverse navbar-fixed-top appnav" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"><strong>Certifacate Course</strong></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse " id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="<?php echo base_url().'home/index'?>">Home</a></li>
        <li></li>
        <li class="leftdist"><a href="<?php echo base_url().'applications/diploma'?>">Diploma</a></li>
        <li class="appactive"><a href="#">Certificate</a></li>
        <li><a href="<?php echo base_url().'applications/certificate'?>">Short Courses</a></li>
        <li><a href="<?php echo base_url(). 'home/programs'?>">Programs</a></li>
        
        <!-- <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Applications<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li class="active"><a href="#">Diploma</a></li>
            <li class="divider"></li>
            <li><a href="#">Bachelor</a></li>
            <li><a href="#">Masters</a></li>  
            <li><a href="#">Doctoral</a></li>
    
          </ul>
        </li> -->
      </ul>
     
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<br/><br/><br/><br/><br/><center>
<h1>STILL UNDER DEVELOPMENT</h1>
</center>
          <script src="<?php echo base_url(). 'assets/flat-ui/js/jquery-1.8.3.min.js'?>"></script>
      <script src="<?php echo base_url(). 'assets/flat-ui/js/jquery-ui-1.10.3.custom.min.js'?>"></script>
      <script src="<?php echo base_url(). 'assets/flat-ui/js/jquery.ui.touch-punch.min.js'?>"></script>
      <script src="<?php echo base_url(). 'assets/flat-ui/js/bootstrap.min.js'?>"></script>
      <script src="<?php echo base_url(). 'assets/flat-ui/js/bootstrap-select.js'?>"></script>
      <script src="<?php echo base_url(). 'assets/flat-ui/js/bootstrap-switch.js'?>"></script>
      <script src="<?php echo base_url(). 'assets/flat-ui/js/flatui-checkbox.js'?>"></script>
      <script src="<?php echo base_url(). 'assets/flat-ui/js/flatui-radio.js'?>"></script>
      <script src="<?php echo base_url(). 'assets/flat-ui/js/jquery.tagsinput.js'?>"></script>
      <script src="<?php echo base_url(). 'assets/flat-ui/js/jquery.placeholder.js'?>"></script>
      <script src="<?php echo base_url(). 'assets/script/jquery/jquery-ui.js'?>"></script>
      <script src="<?php echo base_url(). 'assets/js/main.js'?>"></script>
  </body>
</html>