
<script type="text/javascript">
	$(document).ready(function(){
		$('body').addClass('page-profile');
	});
</script>

<style type="text/css">
	.profile-photo
	{
		border-radius: 0 !important;
	}

	.profile-photo img
	{
		border-radius: 0 !important;
	}

</style>
<div class="profile-full-name">
	<span class="text-semibold"><?php echo $f_name . ' ' . $s_name; ?></span>
</div>

<div class="profile-row">
			<div class="left-col">
				<div class="profile-block">
					<div class="panel profile-photo">
						<img src="<?php echo $profile_picture; ?>" alt="">
					</div><br>
				</div>
				
				<div class="panel panel-transparent">
					<div class="panel-heading">
						<span class="panel-title"><?php echo $f_name . ' ' . $s_name; ?></span>
					</div>
					<div class="panel-body">
						<div class="list-group">
							<a href="#" class="list-group-item"><i class = "ion-card"></i>&nbsp;&nbsp;<span class="text-sm"><?php echo $staff_no;?></span></a>
							<a href="#" class="list-group-item"><i class = "ion-calendar"></i>&nbsp;&nbsp;<span class="text-sm"><?php echo $dob;?></span></a>
							<a href="#" class="list-group-item"><i class = "ion-email"></i>&nbsp;&nbsp;<span class="text-sm"><?php echo $email;?></span></a>
							<a href="#" class="list-group-item"><i class = "ion-iphone"></i>&nbsp;&nbsp;<span class="text-sm"><?php echo $phone_no; ?></span></a>
						</div>
					</div>
				</div>
			</div>
			<div class="right-col">

				<hr class="profile-content-hr no-grid-gutter-h">
				
				<div class="profile-content">
					<div class = "row">
						<div class = "col-md-4">
							<div class="col-md-12">
								<div class="stat-panel">
									<!-- Danger background, vertically centered text -->
									<div class="stat-cell bg-danger valign-middle">
										<!-- Stat panel bg icon -->
										<i class="ion ion-ios-book-outline bg-icon"></i>
										<!-- Extra large text -->
										<span class="text-xlg"><strong><?php echo $units; ?></strong></span><br>
										<!-- Big text -->
										<span class="text-bg">Units</span><br>
										<!-- Small text -->
										<span class="text-sm"><a href="#">View them here</a></span>
									</div> <!-- /.stat-cell -->
								</div> <!-- /.stat-panel -->
							</div>
						</div>
						<div class = "col-md-4">
							<div class="col-md-12">
								<div class="stat-panel">
									<!-- Danger background, vertically centered text -->
									<div class="stat-cell bg-success valign-middle">
										<!-- Stat panel bg icon -->
										<i class="ion ion-android-people bg-icon"></i>
										<!-- Extra large text -->
										<span class="text-xlg"><strong>50</strong></span><br>
										<!-- Big text -->
										<span class="text-bg">Students</span><br>
										<!-- Small text -->
										<span class="text-sm"><a href="#">View them here</a></span>
									</div> <!-- /.stat-cell -->
								</div> <!-- /.stat-panel -->
							</div>
						</div>

						<div class = "col-md-4">
							<div class="col-md-12">
								<div class="stat-panel">
									<!-- Danger background, vertically centered text -->
									<div class="stat-cell bg-info valign-middle">
										<!-- Stat panel bg icon -->
										<i class="ion ion-calendar bg-icon"></i>
										<!-- Extra large text -->
										<span class="text-xlg"><strong><?php echo date('jS'); ?></strong></span><br>
										<!-- Big text -->
										<span class="text-bg"><?php echo date('F Y'); ?></span><br>
										<!-- Small text -->
										<span class="text-sm"><?php echo date('l'); ?></span>
									</div> <!-- /.stat-cell -->
								</div> <!-- /.stat-panel -->
							</div>
						</div>
					</div>
					<h3>Quick Links</h3>
					<hr>
					<div class = "row">
						<div class = "col-md-3">
						<a href = "">
							<div class="stat-panel">
									<!-- Danger background, vertically centered text -->
									<div class="stat-cell bg-info valign-middle">
										<!-- Stat panel bg icon -->
										<i class="ion-ios-printer bg-icon"></i>
										<!-- Extra large text -->
										<span class="text-bg"><strong>Print</strong></span><br>
										<!-- Big text -->
										<span class="text-sm">Class Lists</span><br>
										<!-- Small text -->
										<span class="text-sm">Get a list of your students</span>
									</div> <!-- /.stat-cell -->
								</div> <!-- /.stat-panel -->
							</a>
						</div>

						<div class = "col-md-3">
						<a href = "">
							<div class="stat-panel">
									<!-- Danger background, vertically centered text -->
									<div class="stat-cell bg-danger valign-middle">
										<!-- Stat panel bg icon -->
										<i class="ion-ios-email-outline bg-icon"></i>
										<!-- Extra large text -->
										<span class="text-bg"><strong>Email</strong></span><br>
										<!-- Big text -->
										<span class="text-sm">Administrator</span><br>
										<!-- Small text -->
										<span class="text-sm">Any Problems?</span>
									</div> <!-- /.stat-cell -->
								</div> <!-- /.stat-panel -->
							</a>
						</div>

						<div class = "col-md-3">
							<a href = "">
								<div class="stat-panel">
									<!-- Danger background, vertically centered text -->
									<div class="stat-cell bg-default valign-middle">
										<!-- Stat panel bg icon -->
										<i class="ion-android-upload bg-icon"></i>
										<!-- Extra large text -->
										<span class="text-bg"><strong>Upload</strong></span><br>
										<!-- Big text -->
										<span class="text-sm">Notes</span><br>
										<!-- Small text -->
										<span class="text-sm">Student Notes?</span>
									</div> <!-- /.stat-cell -->
								</div> <!-- /.stat-panel -->
							</a>
						</div>

						<div class = "col-md-3">
							<a href = "">
								<div class="stat-panel">
									<!-- Danger background, vertically centered text -->
									<div class="stat-cell bg-success valign-middle">
										<!-- Stat panel bg icon -->
										<i class="ion-email bg-icon"></i>
										<!-- Extra large text -->
										<span class="text-bg"><strong>Notify</strong></span><br>
										<!-- Big text -->
										<span class="text-sm">Students</span><br>
										<!-- Small text -->
										<span class="text-sm">Keep the students informed</span>
									</div> <!-- /.stat-cell -->
								</div> <!-- /.stat-panel -->
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>